# outputs are primarily for testing
output "vpc_id" {
  value = module.example_vpc.vpc_id
}
output "private_subnet_id" {
  value = module.example_private_subnet.subnet_id
}
output "public_subnet_id" {
  value = module.example_public_subnet.subnet_id
}
