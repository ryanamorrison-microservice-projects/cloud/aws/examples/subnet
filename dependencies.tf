module "example_vpc" {
  source        = "git::https://gitlab.com/ryanamorrison-microservice-projects/cloud/aws/modules/vpc.git"
  vpc_ipv4_cidr = "10.0.0.0/16"
  vpc_tags      = { Name : "example-vpc", Environment : "test" }
}
