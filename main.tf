module "example_private_subnet" {
  source                 = "git::https://gitlab.com/ryanamorrison-microservice-projects/cloud/aws/modules/subnet.git"
  vpc_id                 = module.example_vpc.vpc_id
  subnet_az              = "us-west-2a"
  subnet_ipv4_cidr_block = "10.0.0.0/22"
  subnet_tags            = { Name = "example-public-subnet", Environment = "test" }
}

module "example_public_subnet" {
  source                 = "git::https://gitlab.com/ryanamorrison-microservice-projects/cloud/aws/modules/subnet.git"
  vpc_id                 = module.example_vpc.vpc_id
  subnet_az              = "us-west-2a"
  subnet_ipv4_cidr_block = "10.0.4.0/22"
  subnet_tags            = { Name = "example-private-subnet", Environment = "test" }
  subnet_map_public_ips  = true
}
