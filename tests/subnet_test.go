package test

import(
  "fmt"
  "github.com/gruntwork-io/terratest/modules/aws"
  "github.com/gruntwork-io/terratest/modules/terraform"
  "github.com/stretchr/testify/assert"
  "testing"
)

var awsRegion string = "us-west-2"
var wantSubnetTags = map[string]string{ 
  "Name": "example-subnet",
  "Environment": "test",
}

func TestSubnet(t *testing.T) {
  t.Parallel()

  opts := &terraform.Options{
    TerraformDir: "../",
  }
  defer terraform.Destroy(t, opts)
  terraform.InitAndApply(t, opts)

  privateSubnetId := terraform.OutputRequired(t, opts, "private_subnet_id")
  //publicSubnetId := terraform.OutputRequired(t, opts, "public_subnet_id")
  vpcId := terraform.OutputRequired(t, opts, "vpc_id")
  subnets := aws.GetSubnetsForVpc(t, vpcId, awsRegion)

  assert.Equal(t, subnets[0].Tags["Environment"], wantSubnetTags["Environment"])
  assert.Equal(t, subnets[1].Tags["Environment"], wantSubnetTags["Environment"])
  assert.False(t, aws.IsPublicSubnet(t, privateSubnetId, awsRegion))
  //requires route tables, test after those modules are public and added to dependencies
  //assert.True(t, aws.IsPublicSubnet(t, publicSubnetId, awsRegion))

  //test debugging
  fmt.Println(vpcId)

}  
