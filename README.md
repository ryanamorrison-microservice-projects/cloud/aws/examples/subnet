<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >=1.0.0, < 2.0.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_example_private_subnet"></a> [example\_private\_subnet](#module\_example\_private\_subnet) | git::https://gitlab.com/ryanamorrison-microservice-projects/cloud/aws/modules/subnet.git | n/a |
| <a name="module_example_public_subnet"></a> [example\_public\_subnet](#module\_example\_public\_subnet) | git::https://gitlab.com/ryanamorrison-microservice-projects/cloud/aws/modules/subnet.git | n/a |
| <a name="module_example_vpc"></a> [example\_vpc](#module\_example\_vpc) | git::https://gitlab.com/ryanamorrison-microservice-projects/cloud/aws/modules/vpc.git | n/a |

## Resources

No resources.

## Inputs

No inputs.

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_private_subnet_id"></a> [private\_subnet\_id](#output\_private\_subnet\_id) | n/a |
| <a name="output_public_subnet_id"></a> [public\_subnet\_id](#output\_public\_subnet\_id) | n/a |
| <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id) | outputs are primarily for testing |
<!-- END_TF_DOCS -->